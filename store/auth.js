import Vue from 'vue'

export const state = () => ({
  user: null,
  token: '',
  isLoading: false,
  errors: {},
})

export const mutations = {
  setUser(state, user) {
    Vue.set(state, 'user', user)
  },

  setToken(state, token) {
    state.token = token
  },

  setIsLoading(state, value) {
    state.isLoading = value
  },

  setErrors(state, value) {
    const errorsList = value.reduce((acc, error) => {
      acc[error.field] = error.message
      return acc
    }, {})
    Vue.set(state, 'errors', errorsList)
  },
}

export const actions = {
  async signIn(store, params) {
    try {
      store.commit('setIsLoading', true)
      const {
        data: { token, user },
      } = await this.$axios.post('/auth/sign-in', params)
      store.commit('setToken', token)
      store.commit('setUser', user)
    } catch (e) {
      store.dispatch(
        'toasts/alert',
        { text: e.message, color: 'red darken-1', duration: 3000 },
        { root: true }
      )
      store.commit('setErrors', e.response.data.errors)
    } finally {
      store.commit('setIsLoading', false)
    }
  },
}

export const getters = {
  isAuthenticated: (state) => !!state.token,
  authIsLoading: (state) => state.isLoading,
  validationErrors: (state) => state.errors,
}
