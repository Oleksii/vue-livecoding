export const state = () => ({
  alerts: [],
})

export const mutations = {
  addAlert(state, alert) {
    state.alerts.push(alert)
  },

  shiftAlert(state) {
    state.alerts.shift()
  },
}

export const actions = {
  alert(store, params) {
    store.commit('addAlert', params)
    setTimeout(() => {
      store.commit('shiftAlert')
    }, params.duration || 2000)
  },
}
